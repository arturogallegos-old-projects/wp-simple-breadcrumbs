<?php
/*
Plugin Name: Wp Simple Breadcrumbs
Plugin URI: http://smartcodes.ninja
Description: Shortcode to display a Breadcrumbs in your pages. <code>[wp_breadcrumbs /]</code>
Version: 1.0
Author: arturogallegos
Author URI: http://smartcodes.ninja
License: A "Slug" license name e.g. GPL2
*/
class wp_simple_breadcrumbs{
    public function init(){
        add_shortcode('wp_breadcrumbs', [$this, 'make_bread']);

    }

    public function make_bread(){
        $this->breadcrumbs();
        $menu = array_reverse($this->bread, true);
        $html = '<ul class="breadcrumbs">';
        foreach($menu as $key=>$item){
            $html .= '<li><a href="' . $item['url'] . '">' . $item['title'] . '</a></li>';
        }
        return $html . '</ul>';
    }

    public $bread = array();

    public function breadcrumbs($id = null){
        if(empty($id)){
            global $post;
            $id = $post->ID;
        }
        $post_data = get_post($id);
        $this->bread[$post_data->ID] = ['title' => $post_data->post_title, 'url' => $post_data->guid, 'parent' => $post_data->post_parent];

        if(!empty($post_data->post_parent) && $post_data->post_parent != false){
            $this->breadcrumbs($post_data->post_parent);
        }
        return;
    }
}
$wp_simple_breadcrumbs_init = new wp_simple_breadcrumbs();
$wp_simple_breadcrumbs_init->init();